# import data.data as data
import model.schema
from controller.base import session_factory

class MainModel:
    def __init__(self):
        result = self.get_components(self)
        if result is None:
            session = session_factory()
            session.commit()
            session.close()
            session.close()


    def get_components(self, type):
        # bad programming practice.  I am adding comments after I've forgotten what I am supposed to be doing.
        # This section should get the components from the DB to populate the screens
        session = session_factory()
        if type == "Ore":
            try:
                results = session.query(model.schema.Component, model.schema.OreProdRate).filter(model.schema.Component.component_id == model.schema.OreProdRate.comp_id).all()
                return results
            except:
                return None
        elif type == "Alloy":
            try:
                results = session.query(model.schema.Component, model.schema.AlloyItemProdTime).filter(model.schema.Component.component_id == model.schema.AlloyItemProdTime.comp_id).all()
                return results
            except:
                return None



    def get_single_component(self, name, type):
        session = session_factory()
        try:
            if type == "Ore":
                result = session.query(model.schema.Component, model.schema.OreProdRate).filter(model.schema.Component.component_id == model.schema.OreProdRate.comp_id).filter(model.schema.Component.component_name == name)
            elif type == "Alloy":
                result = session.query(model.schema.Component, model.schema.AlloyItemProdTime).filter(model.schema.Component.component_id == model.schema.AlloyItemProdTime.comp_id).filter(model.schema.Component.component_name == name)

            return result
        except:
            return None



    def add_component(self, **kwargs):
        session = session_factory()
        new_component = model.schema.Component(
            component_name = kwargs.get('component_name'),
            component_type = kwargs.get('component_type'),
            resale_price =  kwargs.get('resale_price'),
            ingredient_one = kwargs.get('ingredient_one'),
            quantity_one = kwargs.get('quantity_one'),
            ingredient_two = kwargs.get('ingredient_two'),
            quantity_two = kwargs.get('quantity_two'),
            ingredient_three = kwargs.get('ingredient_three'),
            quantity_three = kwargs.get('quantity_three')
            )
        session.add(new_component)
        session.commit()

        if kwargs.get('component_type') == 'Ore':
            new_rate = model.schema.OreProdRate(
                prod_rate = kwargs.get('prod_rate'),
                comp_id=new_component.component_id
                )
            session.add(new_rate)
        elif kwargs.get('component_type') == 'Alloy':
            new_time = model.schema.AlloyItemProdTime(
                prod_time = kwargs.get('prod_time'),
                comp_id=new_component.component_id
                )
            session.add(new_time)
        else:
            print("Here is an error")


        session.commit()
        session.close()

    def delete_component(self, del_list):
        session = session_factory()

        for value in del_list:
            result = session.query(model.schema.Component).filter(model.schema.Component.component_name == value).first()
            session.delete(result)
            session.commit()
        session.close()

    def update_component(self, **kwargs):
        session = session_factory()

        print("inside update_component")
        print("kwargs.get(component_id) = ", kwargs.get('component_id'))
        print("kwargs.get(component_name) = ", kwargs.get('component_name'))
        print("kwargs.get('resale_price') = ", kwargs.get('resale_price'))
        print("kwargs.get('prod_rate') = ", kwargs.get('prod_rate'))


#        original = session.query(model.schema.Component, model.schema.OreProdRate). \
#            filter(model.schema.Component.component_id == kwargs.get('component_id')).first()

        original_one = session.query(model.schema.Component). \
            filter(model.schema.Component.component_id == kwargs.get('component_id')).first()

        session.commit()

        original_two = session.query(model.schema.OreProdRate). \
            filter(model.schema.OreProdRate.comp_id == original_one.component_id).first()


        original_one.component_name = kwargs.get('component_name')
        original_one.resale_price = kwargs.get('resale_price')
        original_one.component_type = kwargs.get('component_type')

        if original_one.component_type == "Ore":
            original_two.prod_rate = kwargs.get('prod_rate')


#        original_one.update()
#        original_two.update()
        #session.flush()
        session.commit()
        session.close
