import sqlalchemy
from sqlalchemy import (Column, Float, String, Integer, ForeignKey)
from sqlalchemy.orm import relationship
from src.IdlePlanet.controller.base import Base, engine


class Component(Base):
    # A list of all Ores, Alloys, and Items
    __tablename__ = "component"
    component_id = Column(Integer, primary_key=True, nullable=False,
                          autoincrement=True)

    component_name = Column(String, nullable=False)
    component_type = Column(String, nullable=False)
    resale_price = Column(Float, nullable=False)
    ore_rate = relationship("OreProdRate", cascade="all,delete", backref="component")
    alloy_item_time = relationship("AlloyItemProdTime", cascade="all,delete", backref="component")
    ingredient_one = Column(Integer, ForeignKey("component.component_id"), nullable=True)
    quantity_one = Column(Integer, nullable=True)
    ingredient_two = Column(Integer, ForeignKey("component.component_id"), nullable=True)
    quantity_two = Column(Integer, nullable=True)
    ingredient_three = Column(Integer, ForeignKey("component.component_id"), nullable=True)
    quantity_three = Column(Integer, nullable=True)

class OreProdRate(Base):
    # How much ore is produced per second
    __tablename__ = "ore_prod_rate"
    opr_id = Column(Integer, primary_key=True, nullable=False,
                          autoincrement=True)
    prod_rate = Column(Float,  nullable=False)
    comp_id = Column(Integer, ForeignKey("component.component_id"))

class AlloyItemProdTime(Base):
    # How much time in seconds is required to produce one Alloy or one Item
    __tablename__ = "alloy_item_prod_time"
    aipr_id = Column(Integer, primary_key=True, nullable=False,
                          autoincrement=True)
    prod_time = Column(Float, nullable=False)
    comp_id = Column(Integer, ForeignKey("component.component_id"))

class Recipe(Base):
    __tablename__ = "recipe"
    recipe_id = Column(Integer, primary_key=True, nullable=False,
                          autoincrement=True)
    name = Column(String, nullable=False)

class RecipeComponent(Base):
    __tablename__ = "recipe_component"
    recipe_component_id = Column(Integer, primary_key=True, nullable=False,
                          autoincrement=True)
    component_id = Column(Integer,
                          ForeignKey('component.component_id'), nullable=False)
    recipe_id = Column(Integer,
                          ForeignKey('recipe.recipe_id'), nullable=False)



Base.metadata.create_all(engine)
