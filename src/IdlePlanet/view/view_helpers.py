from PyQt5.QtCore import (QMetaObject,
                                             pyqtSlot,
                                             Qt)

@pyqtSlot()
def show_delete_confirm_window(self):
    count = self.layout_grid.rowCount()

    row = 0
    ore_list = []

# FIXME: Application crashes when attempting to delete more than 1 ore.
# Don't know if this error extends to other issues, yet.
    while row < count:
        info = self.layout_grid.itemAtPosition(row, 0).widget()
        if info.isChecked():
            text = self.layout_grid.itemAtPosition(row, 1).widget().text()
            ore_list.append(text)
        row = row + 1

    self.delete_window = DeleteSelectedWindow(self, self._model, self._main_controller, ore_list)
    self.clear_layout(self.layout_grid)
    ComponentsTabbedView(None, self._model, self._main_controller)
    self.get_comp_info()
