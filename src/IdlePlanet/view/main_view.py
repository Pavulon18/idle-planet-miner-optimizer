from PyQt5.QtWidgets import (QCheckBox,
                                                  QComboBox,
                                                  QDialog,
                                                  QDialogButtonBox,
                                                  QDoubleSpinBox,
                                                  QFormLayout,
                                                  QGridLayout,
                                                  QGroupBox,
                                                  QHBoxLayout,
                                                  QLabel,
                                                  QLineEdit,
                                                  QMainWindow,
                                                  QMessageBox,
                                                  QPushButton,
                                                  QSpinBox,
                                                  QTabWidget,
                                                  QVBoxLayout,
                                                  QWidget)
from PyQt5.QtCore import (QMetaObject,
                                             pyqtSlot,
                                             Qt)
from model.model import MainModel
import view.alloy_view

class MainView(QMainWindow):
    def __init__(self, model, main_controller):
        super(MainView, self).__init__()
        self.model = model
        self.main_controller = main_controller

        self.setWindowTitle("Idle Planet Miner Optimizer")

        # I want a tabbed view with three tabs:
        # Ores, Alloys, and Items

        comp_view_widget = ComponentsTabbedView(self, model, main_controller)
        self.setCentralWidget(comp_view_widget)




class ComponentsTabbedView(QWidget):
    def __init__(self, parent, model, main_controller):
        super(ComponentsTabbedView, self).__init__(parent)
        self._model = model
        self._main_controller = main_controller

        self.layout = QVBoxLayout(self)

        # Initialize Tab Screen
        self.tabs = QTabWidget()
        self.ore_tab = OreTabDisplay(self, model, main_controller)
        self.alloy_tab = AlloyTabDisplay(self, model, main_controller)
        self.item_tab = QWidget()

        # Create Items Tab


        # Add Tabs
        self.tabs.addTab(self.ore_tab, "Ores")
        self.tabs.addTab(self.alloy_tab, "Alloys")
        self.tabs.addTab(self.item_tab, "Items")

        # Add Tabs to Widget
        self.layout.addWidget(self.tabs)

class OreTabDisplay(QWidget):
    def __init__(self, parent, model, main_controller):
        super(OreTabDisplay, self).__init__(parent)

        self._parent = parent
        self._model = model
        self._main_controller = main_controller

        # As I am still trying to wrap my head around the finer points of widgets and
        # layouts and how they relate to each other, this next line is trial and error.
        self.labels_widget = QWidget()

        self.layout_base = QVBoxLayout(self)
        self.layout_labels = QHBoxLayout()
        self.layout_middle = QHBoxLayout()
        self.layout_buttons = QVBoxLayout()
        self.layout_grid = QGridLayout()
        self.layout_grid.setObjectName("layoutGrid")

        # Create Labels
        self.label_name = QLabel("Name")
        self.label_resale_price = QLabel("Resale Price")
        self.label_prod_rate = QLabel("Production Rate \n(Piece / sec)")
        self.label_income_rate = QLabel("Income Rate / sec")

        # Create Main CheckBox
        self.main_checkbox = QCheckBox()

        # At the top of the page, add a buttons
        # Create insert new data Button
        self.button_add_new_ore = QPushButton("New Ore", self)

        # Create delete selected entry button
        self.button_delete_selected = QPushButton("Delete Selected Entries", self)

        # Create the Update Information Button
        self.button_update_info = QPushButton("Update Information", self)

        # Add Buttons to screen
        self.layout_buttons.addWidget(self.button_add_new_ore)
        self.layout_buttons.addWidget(self.button_delete_selected)
        self.layout_buttons.addWidget(self.button_update_info)
        self.layout_base.addLayout(self.layout_buttons)
        self.button_add_new_ore.clicked.connect(self.show_add_new_ore_window)
        self.button_delete_selected.clicked.connect(self.show_delete_confirm_window)
        self.button_update_info.clicked.connect(self.show_update_information_window)

        # Add Labels to lables line
        #self.layout_labels.addWidget(self.main_checkbox)
        self.layout_labels.addWidget(self.label_name)
        self.layout_labels.addWidget(self.label_resale_price)
        self.layout_labels.addWidget(self.label_prod_rate)
        self.layout_labels.addWidget(self.label_income_rate)
        self.labels_widget.setLayout(self.layout_labels)

        # Add labels line to base layout
        self.layout_base.addWidget(self.labels_widget)

        # Add data from database to the view
        # Devise a way to pull data from the database
        self.display_info = self.get_comp_info()


    def get_comp_info(self):
        data_in = MainModel.get_components(self, type = "Ore")

        i = 0

        self.data_out = {}

        if data_in is None:
            self.layout_grid.addWidget(QCheckBox(), 0, 0)
            self.layout_grid.addWidget(QLabel("None"), 0, 1)
            self.layout_grid.addWidget(QLabel("0"), 0, 2)
            self.layout_grid.addWidget(QLabel("0"), 0, 3)
            self.layout_grid.addWidget(QLabel("0"), 0, 4)
        else:
            for row1, row2 in data_in:
                j = 0
                self.layout_grid.addWidget(QCheckBox(), i, j)

                j = j + 1
                self.layout_grid.addWidget(QLabel(row1.component_name), i, j)

                j = j + 1
                self.layout_grid.addWidget(QLabel(str(row1.resale_price)), i, j)


                j = j + 1
                self.layout_grid.addWidget(QLabel(str(row2.prod_rate)), i, j)


                j = j + 1
                dis_rate = str(round(row2.prod_rate * row1.resale_price, 1))
                self.layout_grid.addWidget(QLabel(dis_rate), i, j)

                i = i + 1
        self.layout_base.addLayout(self.layout_grid)
        return self.layout_base

    @pyqtSlot()
    def show_add_new_ore_window(self):
        self.new_window = AddNewOreWindow(self, self._model, self._main_controller)
        self.clear_layout(self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()

    @pyqtSlot()
    def show_delete_confirm_window(self):
        count = self.layout_grid.rowCount()

        row = 0
        ore_list = []

        while row < count:
            info = self.layout_grid.itemAtPosition(row, 0).widget()
            if info.isChecked():
                text = self.layout_grid.itemAtPosition(row, 1).widget().text()
                ore_list.append(text)
            row = row + 1

        self.delete_window = DeleteSelectedWindow(self, self._model, self._main_controller, ore_list)
        self.clear_layout(self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()

    @pyqtSlot()
    def show_update_information_window(self):
        # This will create the window from which the user can change
        # the values of a component.
        count = self.layout_grid.rowCount()

        row = 0

        while row < count:
            info = self.layout_grid.itemAtPosition(row, 0).widget()
            if info.isChecked():
                text = self.layout_grid.itemAtPosition(row, 1).widget().text()

                # Send the user to the window which will confirm that the user
                # actually wants to make changes to that entry.
                ConfirmUpdateWindow(self, self._model, self._main_controller, text)
            row = row + 1

        self.clear_layout(self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()


    def clear_layout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.clearLayout(item.layout())



class AddNewOreWindow(QDialog):
    """
    Displays the modal window to add a new Ore
    """
    def __init__(self, parent, model, main_controller):
        super(AddNewOreWindow, self).__init__()
        self._model = model
        self._main_controller = main_controller

        self.window = QDialog()
        layout = QFormLayout()
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        QMetaObject.connectSlotsByName(self)


        self.name_line_edit = QLineEdit()
        layout.addRow(QLabel("Name"), self.name_line_edit)

        self.resale_value = QDoubleSpinBox()
        self.resale_value.setRange(0, 1000000)
        layout.addRow(QLabel("Resale Value"), self.resale_value)

        self.prod_rate = QDoubleSpinBox()
        self.prod_rate.setRange(0, 10000)
        layout.addRow(QLabel("Production Rate"), self.prod_rate)
        layout.addRow(self.buttonBox)
        self.window.setLayout(layout)
        self.window.exec_()


    @pyqtSlot()
    def accept(self):
        self.new_ore_data = {
                                           'component_name': self.name_line_edit.text(),
                                           'resale_price': self.resale_value.value(),
                                           'component_type': "Ore",
                                           'prod_rate': self.prod_rate.value()
        }
        self._main_controller.new_ore_accept(**self.new_ore_data)
        self.window.repaint()
        self.window.close()

    @pyqtSlot()
    def reject(self):
        #print("Cancel Form")
        self.window.close()

class DeleteSelectedWindow(QMessageBox):
    """
    Displays the modal window to confirm delete selected
    """
    def __init__(self, parent, model, main_controller, delete_list):
        super(DeleteSelectedWindow, self).__init__()
        self._model = model
        self._main_controller = main_controller
        self._delete_list = delete_list

        self.confirmation = QMessageBox.question(self, "Confirm Delete", "Are you sure you want to delete the selected lines?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if self.confirmation == QMessageBox.Yes:
            self._main_controller.delete_ore_entries(self._delete_list)
            return


        else:
            print('No delete Ore.')

        self.show()

class ConfirmUpdateWindow(QMessageBox):
    """
    Displayes the modal window to confirm the changes made to the information
    in the layout_grid
    """

    def __init__(self, parent, model, main_controller, data):
        super(ConfirmUpdateWindow, self).__init__()
        self._parent = parent
        self._model = model
        self._main_controller = main_controller
        self._data = data

        self.confirmation = QMessageBox.question(self, "Confirm Update", "Are you sure you want to update the data?  Will need to confirm that no alloys or items will be affected or list all the allows and items that will be deleted", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if self.confirmation == QMessageBox.Yes:
            #
            # send the user to the window to actually make the changes to the information.
            UpdateOreWindow(self._parent, self._model, self._main_controller, self._data)

        else:
            print('No to confirm Changes to Ore')

        self.show()

class UpdateOreWindow(QDialog):
    """
    Displays the window that will accept user input to change the information.
    """
    def __init__(self, parent, model, main_controller, data):
        super(UpdateOreWindow, self).__init__()
        self._parent = parent
        self._model = model
        self._main_controller = main_controller
        self._data = data

        result = self._model.get_single_component(data, "Ore")
        for item1, item2 in result:
            pass


        self.window = QDialog()
        layout = QFormLayout()
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        QMetaObject.connectSlotsByName(self)

        self.component_id = item1.component_id


        self.name_line_edit = QLineEdit()
        self.name_line_edit.setText(item1.component_name)
        layout.addRow(QLabel("Name"), self.name_line_edit)

        self.resale_value = QDoubleSpinBox()
        self.resale_value.setRange(0, 1000000)
        self.resale_value.setValue(item1.resale_price)
        layout.addRow(QLabel("Resale Value"), self.resale_value)

        self.prod_rate = QDoubleSpinBox()
        self.prod_rate.setRange(0, 10000)
        self.prod_rate.setValue(item2.prod_rate)
        layout.addRow(QLabel("Production Rate"), self.prod_rate)
        layout.addRow(self.buttonBox)
        self.window.setLayout(layout)
        self.window.exec_()

    @pyqtSlot()
    def accept(self):
        # Gathers data from the widgets on screen once the OK button is pressed.
        self.new_ore_data = {
                                           'component_id': self.component_id,
                                           'component_name': self.name_line_edit.text(),
                                           'resale_price': self.resale_value.value(),
                                           'component_type': "Ore",
                                           'prod_rate': self.prod_rate.value()
        }

        # Send the data to the controller
        self._main_controller.update_ore_entries(**self.new_ore_data)
        self.window.repaint()
        self.window.close()

    @pyqtSlot()
    def reject(self):
        #print("Cancel Form")
        self.window.close()


class AlloyTabDisplay(QWidget):
    def __init__(self, parent, model, main_controller):
        super(AlloyTabDisplay, self).__init__(parent)


        self._parent = parent
        self._model = model
        self._main_controller = main_controller


        # Setup some of the layouts and widgets
        self.labels_widget = QWidget()
        self.layout_base = QVBoxLayout(self)
        self.layout_labels = QHBoxLayout()
        self.layout_middle = QHBoxLayout()
        self.layout_buttons = QVBoxLayout()
        self.layout_grid = QGridLayout()
        self.layout_grid.setObjectName("layoutGrid")

        # Create Labels
        self.label_name = QLabel("Name")
        self.label_resale_price = QLabel("Resale Price")
        self.label_prod_time = QLabel("Production Time \n(Time / piece)")
        self.label_income_rate = QLabel("Maximum\nIncome\nRate / sec")

        # At the top of the page, add a buttons
        # Create insert new data Button
        self.button_add_new_alloy = QPushButton("New Alloy", self)

        # Create delete selected entry button
        self.button_delete_selected = QPushButton("Delete Selected Entries", self)

        # Create the Update Information Button
        self.button_update_info = QPushButton("Update Information", self)

        # Create the View Detailed Information Button
        self.button_detailed_info = QPushButton("View Detailed Information", self)

        # Add Buttons to screen
        self.layout_buttons.addWidget(self.button_add_new_alloy)
        self.layout_buttons.addWidget(self.button_delete_selected)
        self.layout_buttons.addWidget(self.button_update_info)
        self.layout_buttons.addWidget(self.button_detailed_info)
        self.layout_base.addLayout(self.layout_buttons)

        # Connect Buttons to Actions
        self.button_add_new_alloy.clicked.connect(self.show_add_new_alloy_window)
        self.button_delete_selected.clicked.connect(self.show_delete_confirm_window)
        self.button_update_info.clicked.connect(self.show_update_information_window)
        self.button_detailed_info.clicked.connect(self.show_detailed_info)


        # Create the Number of Smeters QSpinBox
        self.spinbox_smelters_available = QSpinBox()
        self.spinbox_smelters_available.setRange(0, 5)
        self.smelters_layout = QFormLayout()
        self.smelters_layout.addRow(QLabel("Available Smelters"), self.spinbox_smelters_available)
        self.layout_base.addLayout(self.smelters_layout)


        # Add Labels to lables line
        #self.layout_labels.addWidget(self.main_checkbox)
        self.layout_labels.addWidget(self.label_name)
        self.layout_labels.addWidget(self.label_resale_price)
        self.layout_labels.addWidget(self.label_prod_time)
        self.layout_labels.addWidget(self.label_income_rate)
        self.labels_widget.setLayout(self.layout_labels)

        # Add labels line to base layout
        self.layout_base.addWidget(self.labels_widget)

        # Add data from database to the view
        self.display_info = self.get_comp_info()

    def get_comp_info(self):
        data_in = MainModel.get_components(self, type = "Alloy")

        i = 0

        self.data_out = {}

        if data_in is None:
            self.layout_grid.addWidget(QCheckBox(), 0, 0)
            self.layout_grid.addWidget(QLabel("None"), 0, 1)
            self.layout_grid.addWidget(QLabel("0"), 0, 2)
            self.layout_grid.addWidget(QLabel("0"), 0, 3)
            self.layout_grid.addWidget(QLabel("0"), 0, 4)
        else:
            for row1, row2 in data_in:
                j = 0
                self.layout_grid.addWidget(QCheckBox(), i, j)

                j = j + 1
                self.layout_grid.addWidget(QLabel(row1.component_name), i, j)

                j = j + 1
                self.layout_grid.addWidget(QLabel(str(row1.resale_price)), i, j)


                j = j + 1
                self.layout_grid.addWidget(QLabel(str(row2.prod_time)), i, j)


                j = j + 1
                dis_rate = "Coming"
                self.layout_grid.addWidget(QLabel(dis_rate), i, j)

                i = i + 1
        self.layout_base.addLayout(self.layout_grid)
        return self.layout_base



    @pyqtSlot()
    def show_delete_confirm_window(self):
        count = self.layout_grid.rowCount()

        row = 0
        ore_list = []

        while row < count:
            info = self.layout_grid.itemAtPosition(row, 0).widget()
            if info.isChecked():
                text = self.layout_grid.itemAtPosition(row, 1).widget().text()
                ore_list.append(text)
            row = row + 1

        self.delete_window = DeleteSelectedWindow(self, self._model, self._main_controller, ore_list)
        OreTabDisplay.clear_layout(self, self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()

    @pyqtSlot()
    def show_add_new_alloy_window(self):
        self.new_window = AddNewAlloyWindow(self, self._model, self._main_controller)
        OreTabDisplay.clear_layout(self, self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()

    @pyqtSlot()
    def show_update_information_window(self):
        # This will create the window from which the user can change
        # the values of a component.
        count = self.layout_grid.rowCount()

        row = 0

        while row < count:
            info = self.layout_grid.itemAtPosition(row, 0).widget()
            if info.isChecked():
                line_data = self.layout_grid.itemAtPosition(row, 1).widget().text()

                # Send the user to the window which will confirm that the user
                # actually wants to make changes to that entry.
                UpdateAlloyInformation(self, self._model, self._main_controller, line_data)
            row = row + 1

        OreTabDisplay.clear_layout(self, self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()

    @pyqtSlot()
    def show_detailed_info(self):
        pass

class AddNewAlloyWindow(QDialog):
    """
    Displays the modal window to add a new Ore
    """
    def __init__(self, parent, model, main_controller):
        super(AddNewAlloyWindow, self).__init__()
        self._model = model
        self._main_controller = main_controller

        self.window = QDialog()
        layout = QFormLayout()
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        QMetaObject.connectSlotsByName(self)


        self.name_line_edit = QLineEdit()
        layout.addRow(QLabel("Name"), self.name_line_edit)

        self.resale_value = QDoubleSpinBox()
        self.resale_value.setRange(0, 1000000)
        layout.addRow(QLabel("Resale Value"), self.resale_value)

        self.prod_time = QDoubleSpinBox()
        self.prod_time.setRange(0, 10000)
        layout.addRow(QLabel("Production Time"), self.prod_time)

        # Create Ingredient Row 1
        self.ingredient_one_row = QHBoxLayout()

        self.ingredient_one = QComboBox(self)
        ingred_list = self._main_controller.populate_list("Alloy")
        for item in ingred_list:
            self.ingredient_one.addItem(item)
        self.ingredient_one_row.addWidget(self.ingredient_one)


        self.quantity_one = QSpinBox()
        self.quantity_one.setRange(0, 1000)
        self.ingredient_one_row.addWidget(self.quantity_one)

        layout.addRow(self.ingredient_one_row)

        # Create Ingredient Row 2
        self.ingredient_two_row = QHBoxLayout()

        self.ingredient_two = QComboBox(self)
        ingred_list = self._main_controller.populate_list("Alloy")
        self.ingredient_two.addItem("None")
        for item in ingred_list:
            self.ingredient_two.addItem(item)
        self.ingredient_two_row.addWidget(self.ingredient_two)


        self.quantity_two = QSpinBox()
        self.quantity_two.setRange(0, 1000)
        self.ingredient_two_row.addWidget(self.quantity_two)

        layout.addRow(self.ingredient_two_row)

        # Create Ingredient Row 3
        self.ingredient_three_row = QHBoxLayout()

        self.ingredient_three = QComboBox(self)
        ingred_list = self._main_controller.populate_list("Alloy")
        self.ingredient_three.addItem("None")
        for item in ingred_list:
            self.ingredient_three.addItem(item)
        self.ingredient_three_row.addWidget(self.ingredient_three)


        self.quantity_three = QSpinBox()
        self.quantity_three.setRange(0, 1000)
        self.ingredient_three_row.addWidget(self.quantity_three)

        layout.addRow(self.ingredient_three_row)



        layout.addRow(self.buttonBox)
        self.window.setLayout(layout)
        self.window.exec_()


    @pyqtSlot()
    def accept(self):
        self.new_ore_data = {
                                           'component_name': self.name_line_edit.text(),
                                           'resale_price': self.resale_value.value(),
                                           'component_type': "Alloy",
                                           'prod_time': self.prod_time.value()
        }
        self._main_controller.new_ore_accept(**self.new_ore_data)
        self.window.repaint()
        self.window.close()

    @pyqtSlot()
    def reject(self):
        print("Cancel Form")
        self.window.close()

class UpdateAlloyInformation(QDialog):
    """
    Displays the modal window to add a new Alloy
    """
    def __init__(self, parent, model, main_controller, line_data):
        super(UpdateAlloyInformation, self).__init__()
        self._parent = parent
        self._model = model
        self._main_controller = main_controller
        self._line_data = line_data

        result = self._model.get_single_component(self._line_data, "Alloy")
        for item1, item2 in result:
            pass


        self.window = QDialog()
        layout = QFormLayout()
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        QMetaObject.connectSlotsByName(self)

        self.component_id = item1.component_id

        self.name_line_edit = QLineEdit()
        self.name_line_edit.setText(item1.component_name)
        layout.addRow(QLabel("Name"), self.name_line_edit)

        self.resale_value = QDoubleSpinBox()
        self.resale_value.setRange(0, 1000000)
        self.resale_value.setValue(item1.resale_price)
        layout.addRow(QLabel("Resale Value"), self.resale_value)

        self.prod_time = QDoubleSpinBox()
        self.prod_time.setRange(0, 10000)
        self.prod_time.setValue(item2.prod_time)
        layout.addRow(QLabel("Production Time"), self.prod_time)

        layout.addRow(self.buttonBox)
        self.window.setLayout(layout)
        self.window.exec_()

    @pyqtSlot()
    def accept(self):
        # Gathers data from the widgets on screen once the OK button is pressed.
        self.new_data = {
                                           'component_id': self.component_id,
                                           'component_name': self.name_line_edit.text(),
                                           'resale_price': self.resale_value.value(),
                                           'component_type': "Alloy",
                                           'prod_rate': self.prod_time.value()
        }

        # Send the data to the controller
        self._model.update_component(**self.new_data)
        self.window.repaint()
        self.window.close()

    @pyqtSlot()
    def reject(self):
        #print("Cancel Form")
        self.window.close()
