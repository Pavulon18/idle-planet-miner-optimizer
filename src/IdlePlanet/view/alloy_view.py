from PyQt5.QtWidgets import (QCheckBox,
                                                  QDialog,
                                                  QDialogButtonBox,
                                                  QDoubleSpinBox,
                                                  QFormLayout,
                                                  QGridLayout,
                                                  QGroupBox,
                                                  QHBoxLayout,
                                                  QLabel,
                                                  QLineEdit,
                                                  QMainWindow,
                                                  QMessageBox,
                                                  QPushButton,
                                                  QTabWidget,
                                                  QVBoxLayout,
                                                  QWidget)
from PyQt5.QtCore import (QMetaObject,
                                             pyqtSlot,
                                             Qt)
from model.model import MainModel

class AlloyTabDisplay(QWidget):
    def __init__(self, parent, model, main_controller):
        super(AlloyTabDisplay, self).__init__(parent)


        self._parent = parent
        self._model = model
        self._main_controller = main_controller


        # Setup some of the layouts and widgets
        self.layout_base = QVBoxLayout(self)
        self.layout_labels = QHBoxLayout()
        self.layout_middle = QHBoxLayout()
        self.layout_buttons = QVBoxLayout()
        self.layout_grid = QGridLayout()
        self.layout_grid.setObjectName("layoutGrid")

        # Create Labels
        self.label_name = QLabel("Name")
        self.label_resale_price = QLabel("Resale Price")
        self.label_prod_time = QLabel("Production Time \n(Time / piece)")
        self.label_income_rate = QLabel("Income Rate / sec")

        # At the top of the page, add a buttons
        # Create insert new data Button
        self.button_add_new_alloy = QPushButton("New Alloy", self)

        # Create delete selected entry button
        self.button_delete_selected = QPushButton("Delete Selected Entries", self)

        # Create the Update Information Button
        self.button_update_info = QPushButton("Update Information", self)

        # Add Buttons to screen
        self.layout_buttons.addWidget(self.button_add_new_alloy)
        self.layout_buttons.addWidget(self.button_delete_selected)
        self.layout_buttons.addWidget(self.button_update_info)
        self.layout_base.addLayout(self.layout_buttons)
        self.button_add_new_alloy.clicked.connect(self.show_add_new_alloy_window)
        self.button_delete_selected.clicked.connect(self.show_delete_confirm_window)
        self.button_update_info.clicked.connect(self.show_update_information_window)


        # Add Labels to lables line
        #self.layout_labels.addWidget(self.main_checkbox)
        self.layout_labels.addWidget(self.label_name)
        self.layout_labels.addWidget(self.label_resale_price)
        self.layout_labels.addWidget(self.label_prod_time)
        self.layout_labels.addWidget(self.label_income_rate)
        self.labels_widget.setLayout(self.layout_labels)

        # Add labels line to base layout
        self.layout_base.addWidget(self.labels_widget)

        # Add data from database to the view
        self.display_info = self.get_comp_info()

    @pyqtSlot()
    def show_add_new_alloy_window(self):
        self.new_window = AddNewAlloyWindow(self, self._model, self._main_controller)
        self.clear_layout(self.layout_grid)
        ComponentsTabbedView(None, self._model, self._main_controller)
        self.get_comp_info()
