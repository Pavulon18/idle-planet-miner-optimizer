import sys
from PyQt5.QtWidgets import QApplication
from view.main_view import MainView
from model.model import MainModel
from controller.controller import MainController

class IdlePlanet(QApplication):
    def __init__(self, sys_argv):
        super(IdlePlanet, self).__init__(sys_argv)
        self.model = MainModel()
        self.main_controller = MainController(self.model)
        self.main_view = MainView(self.model, self.main_controller)
        self.main_view.show()





if __name__ == "__main__":
    app = IdlePlanet(sys.argv)
    sys.exit(app.exec_())
