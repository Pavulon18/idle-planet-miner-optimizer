from model.model import MainModel
# from view.main_view import MainView

class MainController:
    def __init__(self, model):
        self.model = MainModel()

    def new_ore_accept(self, **kwargs):
        MainModel.add_component(self, **kwargs)

    def delete_ore_entries(self, delete_list):
        MainModel.delete_component(self, delete_list)

    def update_ore_entries(self, **kwargs):
        MainModel.update_component(self, **kwargs)

    def populate_list(self, type):
        list = []
        results = MainModel.get_components(self, "Ore")
        for item in results:
            list.append(item[0].component_name)


        if type == "Alloy" or "Item":
            results = MainModel.get_components(self, "Alloy")
            for item in results:
                list.append(item[0].component_name)

        if type == "Item":
            results = MainModel.get_components(self, "Item")
            for item in results:
                list.append(item[0].component_name)

        return list
