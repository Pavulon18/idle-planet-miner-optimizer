ores = {"copper" : "ore",
                "iron" : "ore",
                "lead" : "ore",
                "silica" : "ore",
                "aluminum" : "ore",
                "silver" : "ore",
                "gold" : "ore",
                "diamond" : "ore"
                }
alloys = {"copper bar" : "alloy",
                  "iron bar" : "alloy",
                  "lead bar" : "alloy",
                  "silicon bar" : "alloy",
                  "aluminum bar" : "alloy",
                  "silver bar" : "alloy",
                  "gold bar" : "alloy",
                  "bronze alloy" : "alloy"
                  }
items = {"copper wire" : "item",
                  "iron nail" : "item",
                  "battery" : "item",
                  "hammer" : "item",
                  "glass" : "item"
                  }

alloy_recipes = {
        "copper bar" : [10, "copper", 1000, None, None, None, None],
        "iron bar" : [15,  "iron", 1000, None, None, None, None],
        "lead bar" : [20,  "lead", 1000, None, None, None, None],
        "silicon bar" : [31,  "silica", 1000, None, None, None, None],
        "aluminum bar" : [41,  "aluminum", 1000, None, None, None, None],
        "silver bar" : [62,  "silver", 1000, None, None, None, None],
        "gold bar" : [93,  "gold", 1000, None, None, None, None],
        "bronze alloy" : [125, "silver bar",  2,  "copper bar", 10, None, None]
        }
item_recipes = {
        "copper wire" : [33,  "copper bar",  5,  None,  None,  None,  None]
        }


ore_production_rates = {"copper" : 0,
                                    "iron" : 0,
                                    "lead" : 0,
                                    "silica" : 0,
                                    "aluminum" : 0,
                                    "silver" : 0,
                                    "gold" : 0,
                                    "diamond" : 0
                                     }

resale_values = {"copper" : 1.2,
                          "iron" : 3.6,
                          "lead" : 4.8,
                          "silica" : 13,
                          "aluminum" : 20,
                          "silver" : 36,
                          "gold" : 75,
                          "diamond" : 100,
                          "copper bar" : 1740,
                          "iron bar" : 7200,
                          "lead bar" : 8540,
                          "silicon bar" : 17500,
                          "aluminum bar" : 27600,
                          "silver bar" : 60000,
                          "gold bar" : 120000,
                          "bronze alloy" : 234000,
                          "copper wire" : 12000,
                          "iron nail" : 28000,
                          "battery" : 84000,
                          "hammer" : 324000,
                          "glass" : 264000
                          }
