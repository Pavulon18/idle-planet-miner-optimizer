Idle Planet Miner is a mobile game of the idle clicker genre.  The basic premise of the game is the player mines planets for different ores.  These ores can then be smelted into alloys which can then be used to build items.

The whole purpose is to increase the value of the player's universe.  The player may opt to "sell" his universe in exchange for certain bonuses based on the value of the universe at the time of sale.  Selling the universe resets the game to the beginning with the exception of the bonuses.   Those bonuses roll over through each sale.  These bonuses allow the player to more rapidly increase the value of the universe.

As the game progresses, it becomes increasingly more difficult to juggle mining rates of the different planets, the smelter rates, and the item projection rates.

This project is designed to help manage the some of the variables presented in the game.  One of the questions this project aims to answer is what is the most profitable production arrangement.
