#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name="IdlePlanetMinerOptimizer",
    version="0.1",
    description="Simply to chose the best ore / alloy / item combination to maximize profits.",
    long_description="""Simply to choose the best ore / alloy / item combination to maximize profits.""",
    author="Jim Baize",
    author_email="pavulon@hotmail.com",
    maintainer="Jim",
    maintainer_email="pavulon@hotmail.com",
    url="https://gitlab.com/dashboard/projects",
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Development Status :: 1 - Planning"
    ],
    packages=find_packages(),
)
