
def DisplayLists(option,  internalData):
    components = internalData[0]
    #recipes = internalData[1]
    oreProductionRates = internalData[2]
    resaleValues = internalData[3]

    print("Will display all the information about " + option + " currently on hand.")
    tab = tt.Texttable()
    headings = ['Name',  'Type',  'Resale Value', 'Production Rate',  'Income Rate']
    tab.header(headings)
    names = []
    type = []
    resaleValueDisplay = []
    incomeRate = []
    productionRate = []
    for key, value in components.items():
        names.append(key)
        type.append(value)
        resaleValueDisplay.append(resaleValues[key])
        if(value == "ore"):
            productionRate.append(oreProductionRates[key])
        else:
            calculatedProductionRate = ProductionRate(key,  internalData)
            productionRate.append(calculatedProductionRate)

        if(value == "ore"):
            incomeRate.append(OreIncomeRates(key,  internalData))
        else:
            print("float((calculatedProductionRate)) = " + str(float((calculatedProductionRate))))
            print("float(resaleValues[key]) = " + str(float(resaleValues[key])))
            incomeRate.append(float((calculatedProductionRate)) * float(resaleValues[key]))

    for row in zip(names, type, resaleValueDisplay, productionRate, incomeRate):
        tab.add_row(row)

    s = tab.draw()
    print(s)

def AlloyItemIncomeRate(target,  internalData):
    components = internalData[0]
    recipes = internalData[1]
    oreProductionRates = internalData[2]
    resaleValues = internalData[3]

    # Steps to solve this problem
    # 1) Determine which Alloy or Item we are calculating
    # 2) Look up the recipe
    # 3) Determine the limiting factor
        #3 A)  Determine the rate of production of all components involved
            #    I)  Each component may have a list of components
            #    II) Will have to follow the recipes back to the basic ORE
            #    III)  Compute the ORE production rates
            #   IV)  Compute the ALLOY production rates
            #   V)  Continue this process until every component and sub-component's production rates have been calculated
        #3 B) Determine the rate of production of the smelter or crafter, assuming all components are freely available
        #3 C) Find the min of B & C, that will be the limiting factor
    # 4) Determine the product output rate based on limiting factor
    # 5) Determine the income rate based on #4

    targetRecipe = recipes[target]
    targetResaleValue = resaleValues[target]
    print("targetRecipe = " + str(targetRecipe))
    print("targetResaleValue = " + str(targetResaleValue))
    maxProductionRateFinalProduct = 1 / float(targetRecipe[0])
    maxIncomeRateFinalProduct = float(maxProductionRateFinalProduct) * float(targetResaleValue)
    print("maxProductionRateFinalProduct = " + str(maxProductionRateFinalProduct))
    print("maxIncomeRateFinalProduct = " + str(maxIncomeRateFinalProduct))
    #rateCompOne =
        # if compOne != "ORE":
            # get recipe for compOne
    #rateCompTwo =
    #rateCompThree =

def ProductionRate(target,  internalData):
    # maxProductionRate = the rate at which the smelter or crafter can produce output.  This does not
    #    take in to account the rates of the components.

    components = internalData[0]
    recipes = internalData[1]
    oreProductionRates = internalData[2]
    resaleValues = internalData[3]

    if(components[target] == "ore"):
        return oreProductionRates[target]
    else:
        targetRecipe = recipes[target]
        maxProdRate = 1 / float(targetRecipe[0]) # the maximum rate at which one smelter / crafter can produce its output

        print(str(targetRecipe))

        ingredientA = targetRecipe[1]
        ingredientB = []
        ingredientC = []

        if (len(targetRecipe) >= 4):
            ingredientB = targetRecipe[3]
        if (len(targetRecipe) >=6):
            ingredientC = targetRecipe[5]

        rateActualIngredientA = None
        rateActualIngredientB = None
        rateActualIngredientC = None
        rateNeededIngredientA = None
        rateNeededIngredientB = None
        rateNeededIngredientC = None
        ratioA = None
        ratioB = None
        ratioC = None

        if (components[ingredientA] == "ore"):
            # rateIngredientA is the rate at which the needed quantity of A is produced.
            rateActualIngredientA = oreProductionRates[ingredientA]
        else:
            rateActualIngredientA = ProductionRate(ingredientA,  internalData)
        # the following lines are NOT part of the else statement
        rateNeededIngredientA = float(targetRecipe[2]) / float(targetRecipe[0])
        ratioA = float(rateActualIngredientA) / float(rateNeededIngredientA)

        if ingredientB:
            if (components[ingredientB] == "ore"):
                rateActualIngredientB = oreProductionRates[ingredientB]
            else:
                rateActualIngredientB = ProductionRate(ingredientB,  internalData)
            # the following two lines are NOT part of the else statement
            rateNeededIngredientB = float(targetRecipe[4]) / float(targetRecipe[0])
            ratioB = float(rateActualIngredientB) / float(rateNeededIngredientB)

            if ingredientC:
                if (components[ingredientC] == "ore"):
                    rateActualIngredientC = oreProductionRates[ingredientC]
                else:
                    rateActualIngredientC = ProductionRate(ingredientC,  internalData)
                # the following two lines are NOT part of the else statement
                rateNeededIngredientC = float(targetRecipe[6]) / float(targetRecipe[0])
                ratioC = float(rateActualIngredientC) / float(rateNeededIngredientC)

        limitingFactor = ingredientA

        # test for the presence of rateIngredientB
        if ratioB:
            # compare the values of minimumRate and rateIngredientB
            if (ratioB < ratioA):
                limitingFactor = ingredientB
            #test for the presence of rateIngredientC
            #If there is no B then there cannot be a C
            if ratioC:
                if(ratioC < ratioB) and (ratioC < ratioA):
                    limitingFactor = ingredientC

    # so here is the simplified version of stuff that I deleted.
    # This seems to be working correctly, but it is returning a wrong value
    #  I'm wondering if I am returning the value that I am really needing.
        if ratioA >=1:
            print("ratioA >= 1 ratioB Unknown ratioC Unknown")
            print("targetRecipe = " + str(targetRecipe))
            print("limitingFactor = " + str(limitingFactor))
            print("targetRecipe[2] = " + str(targetRecipe[2]))
            print("ProductionRate(limitingFactor) = " + str(ProductionRate(limitingFactor,  internalData)))

            if ratioB:
                if ratioB >= 1:
                    print("ratioA >=1 AND ratioB >=1 ratioC Unknown")

                    if ratioC:
                        if ratioC >= 1:
                            print("ratioA >=1 AND ratioB >=1 AND ratioC >=1")
                            return maxProdRate
                        else:
                            print("ratioA >=1 AND ratioB >=1 AND ratioC <1")
                            return float(ProductionRate[limitingFactor]) / float(targetRecipe[2])
                    else:
                        print("ratioA >=1 AND ratioB >=1 AND ratioC not exist")
                        return maxProdRate
                else:
                    print("ratioA >=1 AND ratioB < 1 AND ratioC does not matter")
                    return float(ProductionRate(limitingFactor,  internalData)) / float(targetRecipe[2])
            else:
                print("ratioA >=1 AND ratioB does not exist AND ratioC does not matter")
                print("targetRecipe = " + str(targetRecipe))
                print("limitingFactor = " + str(limitingFactor))
                print("targetRecipe[2] = " + str(targetRecipe[2]))
                print("ProductionRate[limitingFactor] = " + ProductionRate(limitingFactor,  internalData))
                return maxProdRate

        else:
            print("ratioA < 1 AND ratioB does not matter AND ratioC does not matter")
            print("targetRecipe = " + str(targetRecipe))
            print("limitingFactor = " + str(limitingFactor))
            print("targetRecipe[2] = " + str(targetRecipe[2]))
            print("ProductionRate[limitingFactor] = " + str(ProductionRate(limitingFactor,  internalData)))
    #        return float(oreProductionRates[limitingFactor]) / float(targetRecipe[2])
    # The above line was wrong.  I am trying to the following line to see if it does what I expect it to do.
            return float(ProductionRate(limitingFactor,  internalData) / float(targetRecipe[2]))
            # Ok.  I think I know what I am doing wrong.  I am assuming the product is an ore. I have a couple of different solutions
            #    1)  do an if statement on each one
            #    2) instead of looking up oreProductionRates, send it to ProductionRates and have it check for ORE.




